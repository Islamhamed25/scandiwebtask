$(document).ready(function () {
  /* ****************************************************************** */

  $(".add-form").submit(function (e) {
    var selectedValue = document.getElementById("productType").value;
    if (selectedValue == "none") {
      document.getElementById("typeError").hidden = false;
      return false;
    }
    /*
    $.ajax({
      url: "/oop/Views/Add.php",
      type: "POST",
      success: function () {
        console.log("success worked");
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      },
    });
    */
  });

  /* ****************************************************************** */

  /* ****************************************************************** */

  // for submitting delete products list
  $("#checked-form").submit(function (e) {
    e.preventDefault();
    // Get from elements values
    var data = $(this).serialize();
    $.ajax({
      url: "/Views/delete.php",
      type: "post",
      data: data,
      success: function (data) {
        // console.log(data);
        var arr = JSON.parse(data);
        arr.forEach((element) => {
          $("#" + element)
            .parent()
            .parent()
            .remove();
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      },
    });
  });
  /* ****************************************************************** */
});

// for Switcher type markup html
function getSelectedValue() {
  var selectedValue = document.getElementById("productType").value;
  var book = document.getElementById("Book");
  var dvd = document.getElementById("DVD");
  var furniture = document.getElementById("Furniture");
  var typeError = document.getElementById("typeError");

  var inputSize = document.getElementById("size");
  var inputWeight = document.getElementById("weight");
  var inputWidth = document.getElementById("width");
  var inputLength = document.getElementById("length");
  var inputHeight = document.getElementById("height");

  if (selectedValue == "book") {
    book.hidden = false;
    dvd.hidden = true;
    furniture.hidden = true;

    inputWeight.setAttribute("required", "");
    inputSize.removeAttribute("required");
    inputWidth.removeAttribute("required");
    inputLength.removeAttribute("required");
    inputHeight.removeAttribute("required");

    typeError.hidden = true;
  } else if (selectedValue == "dvd") {
    dvd.hidden = false;
    book.hidden = true;
    furniture.hidden = true;

    inputSize.setAttribute("required", "");

    inputWeight.removeAttribute("required");
    inputWidth.removeAttribute("required");
    inputLength.removeAttribute("required");
    inputHeight.removeAttribute("required");

    typeError.hidden = true;
  } else if (selectedValue == "furniture") {
    furniture.hidden = false;
    dvd.hidden = true;
    book.hidden = true;
    inputWidth.setAttribute("required", "");
    inputLength.setAttribute("required", "");
    inputHeight.setAttribute("required", "");

    inputSize.removeAttribute("required");
    inputWeight.removeAttribute("required");
    typeError.hidden = true;
  }
}
