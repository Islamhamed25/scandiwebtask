<?php   
    include_once './Config/database.php';
    include_once './Models/Models.php';
    include_once './Controllers/ProductController.php';
    include_once './Models/ProductValidator.php';
    
    // Get Database Connection 
    $database = new Database();
    $db = $database->getConnection();
    
    // Product Controller
    $pc = new ProductController($db);
    $products = $pc->GetProducts(); 
    
