<?php 
   include_once '../Config/database.php';
   include_once '../Controllers/ProductController.php';
   include_once '../Models/ProductValidator.php';

    // Get Database Connection 
    $database = new Database();
    $db = $database->getConnection();
    
    // Product Controller
    $pc = new ProductController($db);
    
    if(isset($_POST["checked_box"])) {
        echo json_encode($_POST["checked_box"]);
         $pc->DeleteChecked($_POST["checked_box"]);
    }


?>