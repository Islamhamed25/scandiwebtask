<?php   
    
    include_once './Views/header.php';
    include_once './Views/inc.php';
    
    $product = new Product($db);
     // Post Data 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $validation = new ProductValidator($_POST);
        $errors = $validation->ValidateForm();
        
        $product->sku = $_POST["sku"];
        $product->name = $_POST["name"];
        $product->price = $_POST["price"];
        if( !empty ( $_POST["weight"] ) ) 
            {  
                $book = new Book($db);
                $book->weight = $_POST["weight"];
                $product->book = $book;
                $product->categoryId=1;
        }
        else if( !empty ( $_POST["size"] )  )
        {    
                $dvd = new DVD($db);
                $dvd->size = $_POST["size"];
                $product->dvd = $dvd;
                $product->categoryId=2;
        }
        else { 
            $furniture = new Furniture($db);
            $furniture->width = $_POST["width"];
            $furniture->length = $_POST["length"];
            $furniture->height = $_POST["height"]; 
            $product->furniture = $furniture;
            $product->categoryId=3;
        }  
        if( !sizeof( $errors ) ) {   
            // Add Product to Database
            if(  $pc->Create($product) ) { 
                echo "<script type='text/javascript'>document.location.href='https://islamscandiweb.000webhostapp.com/index.php';</script>";
     
                die();
            }   else { 
               header("Location:https://islamscandiweb.000webhostapp.com/Add.php");
                die();
            } 
        } 
   } 


?>
  
<div class="add-product">
    <div class="container">
        <form id="product_form" class="add-form" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <div class="form-group">
                <label for="sku">SKU</label>
                <input class="form-control" id="sku"  name="sku" required value="<?php 
                    if(isset($_POST)) 
                        echo $product->sku;
                ?>"> 
                <div class="errors">
                    <?php echo $errors["sku"] ?? ' '; ?>
                </div>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" id="name" name="name" required value="<?php 
                    if(isset($_POST)) 
                        echo $product->name;
                ?>">
                <div class="errors">
                    <?php echo $errors["name"] ?? ' '; ?>
                </div>
            </div> 
            <div class="form-group">
                <label for="price">Price</label>
                <input class="form-control" id="price" name="price" required value="<?php 
                        if(isset($_POST)) 
                           echo is_numeric($product->price) ? $product->price: '';
                ?>">
                <div class="errors">
                    <?php echo $errors["price"] ?? ' '; ?>
                </div>
            </div>  
            <div class="form-group">
                <label for="productType">Type Switcher</label>
                <select required id="productType" onchange="getSelectedValue()" class="form-control"  style="width:300px;" >
                    <option value="none" selected disabled>Switcher</option>
                    <option value="book">Book</option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                </select>
               <div class="errors"> <p class="text-center" id="typeError" hidden>please choose the type of product!</p></div>
            </div> 
            <section class="switch-type">
                <div id="Book" hidden> 
                    <div class="form-group">
                        <label for="sizekg">Size (KG)</label>
                        <input class="form-control" id="weight" name="weight" value="<?php 
                            if( isset( $_POST )) 
                                echo $product->book->weight ?? '';
                        ?>">
                        <div class="errors">
                            <?php echo $errors["weight"] ?? '<p style="color:#333;">please provide the weight of book in KG. </p>'; ?>
                        </div>
                    </div> 
                </div>
                <div id="DVD" class="dvd-type" hidden>
                    <div class="form-group">
                        <label for="size-mb">Size (MB)</label>
                        <input class="form-control" id="size" name="size" value="<?php 
                            if(isset($_POST)) 
                                echo  $product->dvd->size ?? '';
                        ?>">
                        <div class="errors">
                        <?php echo $errors["size"] ?? '<p style="color:#333;">please provide the size of book in MB. </p>'; ?>
                        </div>
                     </div>
                </div>
                <div id="Furniture" class="furniture-type" hidden>
                    <div class="form-group">
                        <label for="width">Width (CM)</label>
                        <input class="form-control" id="width" name="width" value="<?php 
                            if(isset($_POST)) 
                                echo $product->furniture->width ?? '';
                        ?>">
                        <div class="errors">
                            <?php echo $errors["width"] ?? ' '; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="length">Length (CM) </label>
                        <input class="form-control" id="length" name="length" value="<?php 
                            if(isset($_POST)) 
                            echo $product->furniture->length ?? '';
                        ?>">
                        <div class="errors">
                            <?php echo $errors["length"] ?? ' '; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="height">Hieght (CM)</label>
                        <input class="form-control" id="height" name="height" value="<?php 
                            if(isset($_POST)) 
                                echo $product->furniture->height ?? '';
                        ?>">
                        <div class="errors">
                            <?php echo $errors["height"] ?? '<p style="color:#333;">please provide the width & height & length </p>'; ?>
                        </div>
                     </div> 
               </div>
            </section>  
            <div class="errors">
                <?php 
                    if(!( empty($errors["weight"]) && empty($errors["size"]) && empty($errors["length"]) && empty($errors["width"]) && empty($errors["height"])) )
                    echo "Your input maybe invaid check again please";
                ?>
            </div>
         </form>
    </div>
</div> 
<?php include_once './Views/footer.php';?>