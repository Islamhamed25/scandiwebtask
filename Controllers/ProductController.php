<?php 
class ProductController { 

    private $conn;
    private $products;
    function __construct($db) { 
        $this->conn = $db;
    } 
    public function GetProducts() { 
        $query = "select * from product Order By Sku";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute() && $stmt->rowCount() > 0)
        {
          $this->products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        return $this->products;
    }  

    public function Create($product) { 

        $query = "INSERT INTO product SET Sku=:sku,Name=:name,Price=:price,Size=:size,width=:width,Height=:height,Length=:length,CategoryId=:categoryId";

        $stmt = $product->conn->prepare($query); 
        // size for database 
        $size = ($product->book->weight == NULL ) ? $product->dvd->size : $product->book->weight ;

        $stmt->bindParam(":sku", $product->sku);
        $stmt->bindParam(":name", $product->name);
        $stmt->bindParam(":price",$product->price );
        $stmt->bindParam(":size", $size); 
        $stmt->bindParam(":width",$product->furniture->width); 
        $stmt->bindParam(":height",$product->furniture->height); 
        $stmt->bindParam(":length",$product->furniture->length); 
        $stmt->bindParam(":categoryId",$product->categoryId); 

        if($stmt->execute()) { 
            return true;
        } else { 
            return false;
        } 
    }

    public function DeleteChecked($CheckedProducts) { 
        // foreach ($CheckedProducts as $product) 
           //  echo $product.'<br>';
        if(!empty($CheckedProducts) ) {
            $skus = join("','",$CheckedProducts);
            $query = "DELETE FROM product WHERE Sku IN ('$skus')";
            $stmt= $this->conn->prepare($query);
            if($stmt->execute()) { 
                return true;
            } else { 
                throw new Exception("Something unexpected happened !!");
            }
        }
    }

    
}

