<?php  

    class ProductValidator { 
        private $data;
        private $errors = []; 

        function __construct($post_data) { 
            $this->data = $post_data;
        }
        public function ValidateForm() {  
            // validate all 
            $this->validateSKU();
            $this->validateName();
            $this->validatePrice();
            $this->Required_Numerical();
            return $this->errors;
        }

        public function validateSKU() { 
            $val = trim($this->data['sku']);
            if(empty($val)) { 
                $this->addError("sku","SKU cannot be empty.");
            } else {
                if(strlen($val) < 6 ) {
                    $this->addError("sku","SKU must be greater than 6 chars.");
                }
            }
        } 
        
        public function validateName() { 
            $val =  htmlspecialchars( trim($this->data['name']) );
            if(empty($val)) { 
                $this->addError("name","Name cannot be empty !!");
            } else { 
                if(strlen($val) < 6 ) {
                    $this->addError("name","Name Must be greater than 6 chars ");
                } 
            }
        } 
        public function validatePrice() { 
            $price = trim($this->data['price']);
            if(empty($price)) { 
                $this->addError("price","price cannot be empty !!");
            } else { 
                if(!is_numeric($price)) 
                    $this->addError("price","Price must be numerical value");   
            }
        }
        public function Required_Numerical() { 
            $types = ['size','weight','width','length','height'];
            $size = trim($this->data['size']);
            $weight = trim($this->data['weight']);
            $width = trim($this->data['width']);
            $length = trim($this->data['length']);
            $height = trim($this->data['height']);

            foreach($types as $type) { 
                if(  $this->data[$type] != NULL && !is_numeric( $this->data[$type] )) 
                     $this->addError($type,"$type must be numerical value");        
            }  
        } 
         
        private function addError($key,$val) {
            $this->errors[$key] = $val;
        }
    }




 