<?php

     // General Product  
     class Product {
        // database connection
        public $conn ;
        // properties name
        public $sku;
        public $name;
        public $price; 
        public $book;
        public $dvd;
        public $furniture;
        public $categoryId;
        function __construct($db) { 
            $this->conn = $db;
            $this->book = new Book($db);
            $this->dvd = new DVD($db);
            $this->furniture = new Furniture($db); 
        } 
    }


    // Book Model
    class Book extends Product
    {
        public $weight; 
        function __construct($db) {
            $this->conn = $db;
        } 
    } 

    // DVD Model 
    class DVD extends Product
    {
        public $size;  
        function __construct($db) {
            $this->conn = $db;
        }
    }
    // Furniture Model 
    class Furniture extends Product
    {
        public $width;
        public $length;
        public $height; 
        function __construct($db) {
            $this->conn = $db;
        }  
    }